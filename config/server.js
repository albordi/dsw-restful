console.log('[Server.js] Configurando o server');
const express = require('express');
const port = process.env.PORT || 3000;
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.listen(port, () => {
    console.log('[Index] Servidor rodando na porta', port);
});

module.exports = app;