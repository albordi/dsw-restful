module.exports = {
    getMovies: (app) => {
        app.get('/api/filmes', (req, res) => {
            res.send('Listar os filmes');
        });
    },
    setMovie: (app) => {
        app.post('/api/filmes', (req, res) => {
            res.send('Incluir um filme');
        });
    },
    getMovie: (app) => {
        app.get('/api/filmes/:id', (req, res) => {
            res.send('Listar um filme');
        });
    },
    putMovie: (app) => {
        app.put('/api/filmes/:id', (req, res) => {
            res.send('Atualiza um filme');
        });
    },
    deleteMovie: (app) => {
        app.delete('/api/filmes/:id', (req, res) => {
            res.send('Apaga um filme');
        });
    }
}